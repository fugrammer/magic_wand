#include "I2Cdev.h"
#include "MPU6050.h"
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif
#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>


RF24 radio(7,8);                    // nRF24L01(+) radio attached using Getting Started board 

RF24Network network(radio);          // Network uses that radio
const uint16_t this_node = 03;        // Address of our node in Octal format
const uint16_t other_node = 00;       // Address of the other node in Octal format
unsigned long last_sent;             // When did we last send?
unsigned long packets_sent;          // How many have we sent already

struct payload_t {                  // Structure of our payload
  unsigned char t;
  int16_t ax;
  int16_t ay;
  int16_t az;
};

MPU6050 accelgyro;
int16_t ax, ay, az;
unsigned long times;
unsigned long start;

void setup() {
    String result=String(0,HEX);
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif
    
    Serial.begin(38400);
    Serial.println(result);
    accelgyro.initialize();
    SPI.begin();
    radio.begin();
    network.begin(/*channel*/ 90, /*node address*/ this_node);
    pinMode(3,INPUT_PULLUP);
    pinMode(13,OUTPUT);
    while (digitalRead(3)==HIGH);
    Serial.println("STARTING!!!");
    delay(1000);
    times=millis();
    start=millis();
}
payload_t payload;
int16_t a[3][25],t=0;
unsigned char c;
String temp;
void loop() {

    if (millis()-times>100){
      times=millis();
      accelgyro.getAcceleration(&ax, &ay, &az);
      a[0][t]=ax;
      a[1][t]=ay;
      a[2][t]=az;
      ++t;
    }
    if (t==25){
      network.update();
      Serial.println("SendingData");
      RF24NetworkHeader header(/*to node*/ other_node);
      String X,Y,Z;
      char X1[50];
      char Y1[16],Y2[16],Y3[16];
      char Z1[16],Z2[16],Z3[16];
      for (int i=0;i<t;i++){
        c=map(a[0][i], -32768, 32767, 0, 255);
        if (c>15) temp=String(c,HEX);
        else temp="0"+String(c,HEX);
        X+=temp;
        c=map(a[1][i], -32768, 32767, 0, 255);
        if (c>15) temp=String(c,HEX);
        else temp="0"+String(c,HEX);
        Y+=temp;
        c=map(a[2][i], -32768, 32767, 0, 255);
        if (c>15) temp=String(c,HEX);
        else temp="0"+String(c,HEX);
        Z+=temp;
      }
      Serial.println(X);
      Serial.println(Y);
      Serial.println(Z);
      delay(100);
      network.update();
       for (int i=0;i<50;i++){
       X1[i]=X[i];
        Y1[i]=Y[i];
        Z1[i]=Z[i];
      }
      bool ok= network.write(header,&X1,50);
      delay(100);
      ok= network.write(header,&Y1,50);
      delay(100);
      ok= network.write(header,&Z1,50);
      if (ok)
        Serial.println("ok.");
      else
        Serial.println("failed.");

      
//      network.write(header,&X1,16);
//      network.write(header,&Y1,16);
//      network.write(header,&Z1,16);
//      for (int i=16;i<32;i++){
//        X2[i-16]=X[i];
//        Y2[i-16]=Y[i];
//        Z2[i-16]=Z[i];
//      }
//
//      delay(500);
//      for (int i=32;i<48;i++){
//        X3[i-32]=X[i];
//        Y3[i-32]=Y[i];
//        Z3[i-32]=Z[i];
//        Serial.println(X3[i-32]);
//      }
//      delay(500);
//      network.write(header,&X3,16);
//      network.write(header,&Y3,16);
//      network.write(header,&Z3,16);
//      delay(500);
      Serial.println("Done");
      while(1){
        network.update();
      }
    }

}
