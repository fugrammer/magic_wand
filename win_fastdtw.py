import numpy as np
import serial
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw

#Communication with Arduino
ser = serial.Serial('COM3', 38400)

#Arrays to store the gestures
Circle=[]
Figure=[]
Special=[]
Test=[]

#Read pre-recorded gestures from database file
def readDatabase():
    global Circle
    global Figure
    global Special
    global Test
    f = open("database.txt", "r")
    s = f.readline()
    while 1:
        s = f.readline();
        if s[0] == "F":
            break
        x = [int(i) for i in s.split(',')]
        s = f.readline();
        y = [int(i) for i in s.split(',')]
        s = f.readline();
        z = [int(i) for i in s.split(',')]
        temp=[]
        for i in range(0,len(x)):
            temp+=[[x[i],y[i],z[i]]]
        Circle += [temp]
    while 1:
        s = f.readline();
        if s[0] == "S":
            break
        x = [int(i) for i in s.split(',')]
        s = f.readline();
        y = [int(i) for i in s.split(',')]
        s = f.readline();
        z = [int(i) for i in s.split(',')]
        temp=[]
        for i in range(0,len(x)):
            temp+=[[x[i],y[i],z[i]]]
        Figure += [temp]
    while 1:
        s = f.readline();
        if s[0] == "L":
            break
        x = [int(i) for i in s.split(',')]
        s = f.readline();
        y = [int(i) for i in s.split(',')]
        s = f.readline();
        z = [int(i) for i in s.split(',')]
        temp=[]
        for i in range(0,len(x)):
            temp+=[[x[i],y[i],z[i]]]
        Special += [temp]

#Converting waveforms values from -15k to 15k to -1 to 1
def normalise(array):
    a = max(array)
    b = min(array)
    c = max([abs(a), abs(b)])
    for i in range(0,len(array)):
        array[i]=array[i]/float(c)
def normaliseAll():
    for i in Circle:
        for o in i:
            normalise(o)
    for i in Figure:
        for o in i:
            normalise(o)
    for i in Special:
        for o in i:
            normalise(o)
    for i in Test:
        for o in i:
            normalise(o)


def initialise():
    normaliseAll()
    for i in range(0, len(Circle)):
        Circle[i] = np.array(Circle[i])
    for i in range(0, len(Figure)):
        Figure[i] = np.array(Figure[i])
    for i in range(0, len(Special)):
        Special[i] = np.array(Special[i])


#Match measured waveform to pre-recorded gestures
def check(Test):
    circleSum=-1
    for i in range(0, len(Circle)):
        sum = 0
        x = Circle[i]
        y = Test[0]
        dist, path = fastdtw(x, y, dist=euclidean)
        sum += dist
        if circleSum==-1 or sum<circleSum:
            circleSum=sum
    figureSum=-1
    for i in range(0, len(Figure)):
        sum = 0
        x = Figure[i]
        y = Test[0]
        dist, path = fastdtw(x, y, dist=euclidean)
        sum += dist
        if figureSum==-1 or sum<figureSum:
            figureSum=sum
    specialSum=-1
    for i in range(0, len(Special)):
        sum = 0
        x = Special[i]
        y = Test[0]
        dist, path = fastdtw(x, y, dist=euclidean)
        sum += dist
        if specialSum==-1 or sum<specialSum:
            specialSum=sum
    print circleSum,figureSum,specialSum
    if circleSum<figureSum and circleSum<specialSum:
        print "Circle"
        return 0
    elif figureSum<circleSum and figureSum<specialSum:
        print "Figure of 8"
        return 1
    else:
        print "Special"
        return 2


def getData():
    Test=[]
    s = ser.readline();
    x = [int(i) for i in s.split(',')]
    s = ser.readline();
    y = [int(i) for i in s.split(',')]
    s = ser.readline();
    z = [int(i) for i in s.split(',')]
    temp = []
    for i in range(0, len(x)):
        temp += [[x[i], y[i], z[i]]]
    Test += [temp]
    return Test

def main():
    while 1:
        print "Waiting"
        #Read data from Arduino
        Test=getData()
        #Process gesture
        for i in Test:
            for o in i:
                normalise(o)
        for i in range(0, len(Test)):
            Test[i] = np.array(Test[i])
        print "checking"
        #Check gesture
        check(Test)

readDatabase()
initialise()
main()
