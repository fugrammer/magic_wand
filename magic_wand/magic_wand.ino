#include "I2Cdev.h"
#include "MPU6050.h"
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

MPU6050 accelgyro;
//MPU6050 accelgyro(0x69); // <-- use for AD0 high
int16_t ax, ay, az;
unsigned long times;
unsigned long start;
void setup() {
    // join I2C bus (I2Cdev library doesn't do this automatically)
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    Serial.begin(38400);
    accelgyro.initialize();
    pinMode(3,INPUT_PULLUP);
    pinMode(13,OUTPUT);
    digitalWrite(13,LOW);
    delay(2000);
    digitalWrite(13,HIGH);
    times=millis();
    start=millis();
}
long a[3][50],t=0;
void loop() {

    if (millis()-times>100){
      times=millis();
      accelgyro.getAcceleration(&ax, &ay, &az);
      a[0][t]=ax;
      a[1][t]=ay;
      a[2][t]=az;
      ++t;
    }
    if (millis()-start>4000){
      digitalWrite(13,LOW);
      for (int o=0;o<3;o++){
        for (int i=0;i<t;i++){
          Serial.print(a[o][i]);
          if (t-i>1)
          Serial.print(",");
        }
        Serial.println();
      }
      while(1){
      }
    }

}
