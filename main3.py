import numpy as np
from dtw import dtw
import serial
import Tkinter as tk
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw

x = np.array([[1,1,1], [2,2,2]])
y = np.array([[1,1,1], [3,3,3]])
distance, path = fastdtw(x, y, dist=euclidean)
print(distance)


root=tk.Tk()
root.title("Magic Wand")
labels=[]
labels+=[tk.Label(root,bg="white")]
labels+=[tk.Label(root,bg="white")]
labels+=[tk.Label(root,bg="white")]
labels[0].config(text="Circle")
labels[1].config(text="Figure of 8")
labels[2].config(text="Special")
for i in labels:
    i.config(font="Times 100 bold")
    i.pack()

ser = serial.Serial('/dev/ttyUSB0', 38400)
Circle=[]
Figure=[]
Special=[]
Test=[]

def readDatabase():
    global Circle
    global Figure
    global Special
    global Test
    f = open("database.txt", "r")
    s = f.readline()
    while 1:
        s = f.readline();
        if s[0] == "F":
            break
        x = [int(i) for i in s.split(',')]
        s = f.readline();
        y = [int(i) for i in s.split(',')]
        s = f.readline();
        z = [int(i) for i in s.split(',')]
        Circle += [[x, y, z]]
    while 1:
        s = f.readline();
        if s[0] == "S":
            break
        x = [int(i) for i in s.split(',')]
        s = f.readline();
        y = [int(i) for i in s.split(',')]
        s = f.readline();
        z = [int(i) for i in s.split(',')]
        Figure += [[x, y, z]]
    while 1:
        s = f.readline();
        if s[0] == "L":
            break
        x = [int(i) for i in s.split(',')]
        s = f.readline();
        y = [int(i) for i in s.split(',')]
        s = f.readline();
        z = [int(i) for i in s.split(',')]
        Special += [[x, y, z]]
def normalise(array):
    a = max(array)
    b = min(array)
    c = max([abs(a), abs(b)])
    for i in range(0,len(array)):
        array[i]=array[i]/float(c)
def normaliseAll():
    for i in Circle:
        for o in i:
            normalise(o)
    for i in Figure:
        for o in i:
            normalise(o)
    for i in Special:
        for o in i:
            normalise(o)
    for i in Test:
        for o in i:
            normalise(o)
def initialise():
    normaliseAll()
    for i in range(0, len(Circle)):
        for o in range(0, 3):
            Circle[i][o] = np.array(Circle[i][o]).reshape(-1, 1)
    for i in range(0, len(Figure)):
        for o in range(0, 3):
            Figure[i][o] = np.array(Figure[i][o]).reshape(-1, 1)
    for i in range(0, len(Special)):
        for o in range(0, 3):
            Special[i][o] = np.array(Special[i][o]).reshape(-1, 1)
def check(Test):
    circleSum=-1
    for i in range(0, 1):
        sum = 0
        for o in range(0, 3):
            x = Circle[i][o]
            y = Test[0][o]
            dist, cost, acc, path = dtw(x, y, dist=lambda x, y: np.linalg.norm(x - y, ord=1))
            sum += dist
        if circleSum==-1 or sum<circleSum:
            circleSum=sum
    figureSum=-1
    for i in range(0, 1):
        sum = 0
        for o in range(0, 3):
            x = Figure[i][o]
            y = Test[0][o]
            dist, cost, acc, path = dtw(x, y, dist=lambda x, y: np.linalg.norm(x - y, ord=1))
            sum += dist
        if figureSum==-1 or sum<figureSum:
            figureSum=sum
    specialSum=-1
    for i in range(0, 1):
        sum = 0
        for o in range(0, 3):
            x = Special[i][o]
            y = Test[0][o]
            dist, cost, acc, path = dtw(x, y, dist=lambda x, y: np.linalg.norm(x - y, ord=1))
            sum += dist
        if specialSum==-1 or sum<specialSum:
            specialSum=sum
    print circleSum,figureSum,specialSum
    if circleSum<figureSum and circleSum<specialSum:
        print "Circle"
        return 0
    elif figureSum<circleSum and figureSum<specialSum:
        print "Figure of 8"
        return 1
    else:
        print "Special"
        return 2
def main():
    previous=0
    while 1:
        Test = []
        print "Waiting"
        s = ser.readline()
        while s=="":
			root.update()
			s=ser.readline()
        x = [int(i) for i in s.split(',')]
        s = ser.readline();
        y = [int(i) for i in s.split(',')]
        s = ser.readline();
        z = [int(i) for i in s.split(',')]
        Test += [[x, y, z]]
        for i in Test:
            for o in i:
                normalise(o)
        for i in range(0, len(Test)):
            for o in range(0, 3):
                Test[i][o] = np.array(Test[i][o]).reshape(-1, 1)
        print "checking"
        labels[previous].config(bg="white")
        previous=check(Test)
        labels[previous].config(bg="red")
        root.update()
readDatabase()
initialise()
main()
