import numpy as np
from dtw import dtw
import serial
import Tkinter as tk
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw

# root=tk.Tk()
# root.title("Magic Wand")
# labels=[]
# labels+=[tk.Label(root,bg="white")]
# labels+=[tk.Label(root,bg="white")]
# labels+=[tk.Label root,bg="white")]
# labels[0].config(text="Circle")
# labels[1].config(text="Figure of 8")
# labels[2].config(text="Special")
# for i in labels:
#     i.config(font="Times 100 bold")
#     i.pack()

ser = serial.Serial('/dev/ttyUSB0', 38400)
Circle=[]
Figure=[]
Special=[]
Test=[]

def readDatabase():
    global Circle
    global Figure
    global Special
    global Test
    f = open("database.txt", "r")
    s = f.readline()
    while 1:
        s = f.readline();
        if s[0] == "F":
            break
        x = [int(i) for i in s.split(',')]
        s = f.readline();
        y = [int(i) for i in s.split(',')]
        s = f.readline();
        z = [int(i) for i in s.split(',')]
        temp=[]
        for i in range(0,len(x)):
            temp+=[[x[i],y[i],z[i]]]
        Circle += [temp]
    while 1:
        s = f.readline();
        if s[0] == "S":
            break
        x = [int(i) for i in s.split(',')]
        s = f.readline();
        y = [int(i) for i in s.split(',')]
        s = f.readline();
        z = [int(i) for i in s.split(',')]
        temp=[]
        for i in range(0,len(x)):
            temp+=[[x[i],y[i],z[i]]]
        Figure += [temp]
    while 1:
        s = f.readline();
        if s[0] == "L":
            break
        x = [int(i) for i in s.split(',')]
        s = f.readline();
        y = [int(i) for i in s.split(',')]
        s = f.readline();
        z = [int(i) for i in s.split(',')]
        temp=[]
        for i in range(0,len(x)):
            temp+=[[x[i],y[i],z[i]]]
        Special += [temp]
def normalise(array):
    a = max(array)
    b = min(array)
    c = max([abs(a), abs(b)])
    for i in range(0,len(array)):
        array[i]=array[i]/float(c)
def normaliseAll():
    for i in Circle:
        for o in i:
            normalise(o)
    for i in Figure:
        for o in i:
            normalise(o)
    for i in Special:
        for o in i:
            normalise(o)
    for i in Test:
        for o in i:
            normalise(o)
def initialise():
    normaliseAll()
    for i in range(0, len(Circle)):
        Circle[i] = np.array(Circle[i])
    for i in range(0, len(Figure)):
        Figure[i] = np.array(Figure[i])
    for i in range(0, len(Special)):
        Special[i] = np.array(Special[i])

def check(Test):
    circleSum=-1
    for i in range(0, len(Circle)):
        sum = 0
        x = Circle[i]
        y = Test[0]
        dist, path = fastdtw(x, y, dist=euclidean)
        sum += dist
        if circleSum==-1 or sum<circleSum:
            circleSum=sum
    figureSum=-1
    for i in range(0, len(Figure)):
        sum = 0
        x = Figure[i]
        y = Test[0]
        dist, path = fastdtw(x, y, dist=euclidean)
        sum += dist
        if figureSum==-1 or sum<figureSum:
            figureSum=sum
    specialSum=-1
    for i in range(0, len(Special)):
        sum = 0
        x = Special[i]
        y = Test[0]
        dist, path = fastdtw(x, y, dist=euclidean)
        sum += dist
        if specialSum==-1 or sum<specialSum:
            specialSum=sum
    print circleSum,figureSum,specialSum
    if circleSum<figureSum and circleSum<specialSum:
        print "Circle"
        return 0
    elif figureSum<circleSum and figureSum<specialSum:
        print "Figure of 8"
        return 1
    else:
        print "Special"
        return 2

def main():
    previous=0
    while 1:
        Test = []
        print "Waiting"
        s = ser.readline();
        #while s=="":
            #root.update()
            #s=ser.readline()
        #print "done"
        #print s
        x = [int(i) for i in s.split(',')]
        s = ser.readline();
        y = [int(i) for i in s.split(',')]
        s = ser.readline();
        z = [int(i) for i in s.split(',')]
        temp=[]
        for i in range(0,len(x)):
            temp+=[[x[i],y[i],z[i]]]
        Test += [temp]

        #print Test[0]
        for i in Test:
            for o in i:
                normalise(o)
        for i in range(0, len(Test)):
            Test[i] = np.array(Test[i])
        print "checking"
        #labels[previous].config(bg="white")
        previous=check(Test)
        #labels[previous].config(bg="red")
        #root.update()
readDatabase()
initialise()
main()
